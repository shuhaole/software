from top2vec import Top2Vec

max_lines = 1000000
a = []
file_name = "1ml_titles/1ml_titles.csv"
counter = 0

with open(file_name, "r") as f:
    for i in f.readlines():
        if counter == max_lines:
            break
        a.append(i[:-1])
        counter += 1

model = Top2Vec(a[:max_lines], speed="learn", workers=64)
model.save("1m_model")

### Next pipeline step

# topic_words, word_scores, topic_scores, topic_nums = model.search_topics(keywords=["security"], num_topics=10)
#
# sizes, num = model.get_topic_sizes()
# with open("/pygrams/data/topic_words.csv", "w") as f:
#     f.write("abstract\n")
#     for j in topic_nums:
#         documents, document_scores, document_ids = model.search_documents_by_topic(topic_num=j, num_docs=sizes[j])
#         for i in documents:
#             f.write(i + "\n")

### run pygrams using:
# python /pygrams/pygrams.py -ds=topic_words.csv -mx=2 -mx=3 -o wordcloud
